<?php

use Illuminate\Support\Facades\Auth;

Route::group(['middleware' => ['auth'], 'namespace' => 'Aluno'], function () {
    // rota Perfil
    Route::get('/painel/aluno/meu-perfil', 'UserController@perfil')->name('painel.aluno.perfil');
    Route::get('/painel/aluno/senhas', 'UserController@perfil')->name('painel.aluno.senhas');
    // rota abertura protocolo
    Route::post('/painel/protocolo/abertura', 'ProtocolosController@abertura')->name('painel.protocolo.abertura');
    // rotas aluno
    Route::get('/painel/aluno/boletim', 'BoletimController@index')->name('painel.aluno.boletim');
    Route::get('/painel/aluno/protocolos', 'ProtocolosController@listaprotocolo')->name('painel.aluno.protocolos');

    // home do painel
    Route::get('/painel', 'HomeController@indexAluno')->name('painel.home.index');
    // situação curricular
    Route::get('/painel/situacao', 'SituacaoController@index')->name('painel.aluno.situacao');
    // Atividades Complementares
    Route::get('/painel/atividades', 'AtividadesComplementaresController@index')->name('painel.aluno.atividades');
    // Links úteis
    Route::get('/painel/links', 'linksController@index')->name('painel.aluno.links');
    // Configurações
    Route::get('/painel/aluno/config', 'ConfigController@index')->name('painel.aluno.config');
    Route::post('/painel/aluno/config/image', 'ConfigController@trocaImagePerfil')->name('painel.aluno.config.trocaImagePerfil');
    Route::post('/painel/aluno/config/imagefundo', 'ConfigController@trocaImageFundo')->name('painel.aluno.config.trocaImageFundo');
    Route::post('/painel/aluno/config/senha', 'ConfigController@mudaSenha')->name('painel.config.senha');
    Route::post('/painel/aluno/config/endereco', 'ConfigController@mudaEndereco')->name('painel.config.endereco');
    // logout
    Route::get('/logout', function(){
        Auth::logout();
        return redirect('login');
    });
    Route::get('/config', 'ConfigController@index')->name('config');
});
// Route::get('/', function(){
//     return view('admin.teste');
// })->name('admin.home.teste');

Route::group(['middleware' => ['auth'], 'namespace' => 'Dashboard'], function(){
    Route::get('/dashboard', 'DashboardController@index')->name('url_dashboard');
    Route::get('/dashboard/lista', 'DashboardController@listaUser')->name('url_admin_lista');
    Route::get('/dashboard/menu', 'DashboardController@index')->name('admin.menu');
    Route::get('/dashboard/settings', 'DashboardController@config')->name('admin.settings');
    Route::get('/dashboard/profile', 'DashboardController@profile')->name('admin.profile');
    Route::get('/dashboard/cadastro', 'DashboardController@cadastro')->name('url_admin_cadastro');
    Route::get('/dashboard/home', 'DashboardController@home')->name('admin.home');
    Route::get('/dashboard/settings/senhas', 'DashboardController@senhas')->name('admin.settings.senhas');
    Route::get('/dashboard/calendar', 'DashboardController@calendar')->name('url_admin_calendar');
    Route::get('/dashboard/lista/user/{id}/acao/delete', 'DashboardController@listaUser')->name('url_admin_deletar_user');
    Route::get('/dashboard/chat', 'DashboardController@chat')->name('url_admin_chat');
    Route::get('/dashboard/protocolo', 'DashboardController@protocolo')->name('url_admin_protocolo');
    Route::post('/dashboard/novo/user', 'Admin\CadastrosController@create')->name('url_admin_cadastro_create');
});


//Route::get('/dashboard/calendar', 'DashboardController@calendar')->name('admin.calendar');
// Route::get('/ava', 'Site\SiteController@index')->name('home.site');
Route::get('/login', 'HomeController@index')->name('home.site');
Auth::routes();
