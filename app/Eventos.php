<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{
    protected $fillable = [
        'name',
        'detalhes',
        'start',
        'end',
        'color',
        'background',
        'status',
        'user_id',
    ];
}
