<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {
        $name = "Dashboard name";
        return view('admin.index', compact('name'));
    }

    public function profile()
    {
        return view('admin.profile');
    }

    public function config()
    {
        return view('admin.settings');
    }

    public function cadastro()
    {
        return view('admin.cadastro');
    }

    public function home()
    {
        return view('admin.index');
    }

    public function senhas()
    {
        return view('admin.senhas');
    }

    public function calendar()
    {
        return view('admin.calendar');
    }

    public function listaUser()
    {
//        $nomes = ['Jefferson', 'João', 'Clauzi', 'Paula'];
//        $id = 1;
        $nomes = User::all();
        return view('admin.lista', compact('nomes', 'dataCadastro'));
    }

    public function chat()
    {
        $users = User::all();

//        dd($users);
        return view('admin.chat', compact('users'));
    }
}
