<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Alertas;
use App\Cursos;
class UserController extends Controller
{
    //Todas as informações do perfil do usuário
    public function perfil(){
    	$message = 5;
    	$dt = date("i", strtotime('i'));
    	$alertas = Alertas::get();
    	return view('admin.perfil', compact('message', 'dt', 'alertas'));
    }
    public function message(){
    	$message = 5;
    	$dt = date("i");
    	$alertas = Alertas::all();
    	return view('layouts.message', compact('message', 'dt', 'alertas'));
    }

    public function listaDisciplina(){
        $message = 5;
        $dt = date("i");
        $totalcursos = Cursos::all();
        return view('admin.teste', compact('message', 'dt', 'totalcursos'));
    }
    public function menu(){
        $message = 5;
        $dt = date("i");
        $totalcursos = Cursos::all();
        return view('admin.teste', compact('message', 'dt', 'totalcursos'));
    }
}
