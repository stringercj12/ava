<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class CadastrosController extends Controller
{
    private $user;

    public function __construct(User $users)
    {
        $this->user = $users;
    }

    public function index()
    {
        return view('admin.cadastro');
    }

    public function listaUser()
    {
        $nomes = User::all();
        return view('admin.lista', compact('nomes'));
    }

    public function create(Request $request)
    {
//        $validated = $this->validate($request, 'email.required', 'Erro no email');
        dd($request->all());
        $user = $this->user->create($request->all());

        if ($user) {
            return redirect()
                ->route('url_admin_cadastro')
                ->with('success', 'Cadastrado com sucesso');
        }

    }
}
