<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{
    public function chat(){
        $nameChat = 'Paula Ferreira Dos Reis';
        return view('admin.chat', compact('nameChat'));
    }
}
