<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cursos;

class BoletimController extends Controller
{
    public function index(){
    	$totalcursos = Cursos::all();
    	return view('admin.boletim', compact('totalcursos'));
    }
}
