<?php

namespace App\Http\Controllers\Aluno;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Alertas;
use App\Cursos;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function indexAluno(){
        $eventos = \App\Eventos::where('status','!=', '')->limit(3)->get();
        // $eventos = \App\Eventos::all();
        return view('home.index', compact('eventos'));
    }
}
