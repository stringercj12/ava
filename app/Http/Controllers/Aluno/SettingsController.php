<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function settings (){
        return view('admin.settings');
    }

    public function senhas(){
        return view('admin.senhas');
    }

    public function alteraDados(Request $Request){
        dd($Request);

        return 'Alterar dados';
    }

    public function profile(Request $Request){
        return view('admin.profile');
    }
}
