<?php

namespace App\Http\Controllers\Aluno;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;


class ConfigController extends Controller
{
  public function index(){
    return view('aluno.perfil');
  }
  public function trocaImagePerfil(Request $request){


    $nameFile = null;

    if($request->hasFile('trocaImagePerfil') && $request->file('trocaImagePerfil')->isValid()){
      $name = auth()->user()->id."-".uniqid(date('HisYmd'));

      $extension = $request->trocaImagePerfil->extension();

      $nameFile = "{$name}.{$extension}";

      $upload = $request->trocaImagePerfil->storeAs('imageperfil',$nameFile);
      $nameFile = "http://ava.jcf.com/storage/imageperfil/".$nameFile;
      $user = User::find(auth()->user()->id);
      $user->image = $nameFile;
      $user->save();

      if($upload && $user){
        return redirect()
            ->route('painel.aluno.config')
            ->with('success', 'Imagem alterada com sucesso');
      }else{
        return redirect()
            ->back()
            ->with('error', 'Falha ao trocar Imagem de perfil');
      }
    }
  }

  public function trocaImageFundo(Request $request){


    $nameFile = null;

    if($request->hasFile('trocaImageFundo') && $request->file('trocaImageFundo')->isValid()){
      $name = auth()->user()->id."-".uniqid(date('HisYmd'));

      $extension = $request->trocaImageFundo->extension();

      $nameFile = "{$name}.{$extension}";

      $upload = $request->trocaImageFundo->storeAs('temafundo',$nameFile);
      $nameFile = "http://ava.jcf.com/storage/temafundo/".$nameFile;
      $user = User::find(auth()->user()->id);
      $user->temafundo = $nameFile;
      $user->save();

      if($upload && $user){
        return redirect()
            ->route('painel.aluno.config')
            ->with('success', 'Tema de Fundo alterado com sucesso');
      }else{
        return redirect()
            ->back()
            ->with('error', 'Falha ao trocar Tema de Fundo');
      }
    }
  }

  public function mudaSenha(Request $request){

    $user = User::find(auth()->user()->id);

    if($request->antiga_senha != $user->password){
      return redirect()
          ->back()
          ->with('error', 'Antiga senha inválida');
    }

    if($request->antiga_senha == " "){
      return redirect()
          ->back()
          ->with('error', 'Digite sua antiga senha');
    }

    if($request->nova_senha != $request->confirma_senha){

      return redirect()
          ->back()
          ->with('error', 'As novas senhas não combinam');

    }

    if($request->nova_senha == $request->confirma_senha){

      $user->password = bcrypt($request->nova_senha);
      $user->save();

      if($user){
        return redirect()
            ->route('painel.aluno.config')
            ->with('success', 'Senha Alterada com sucesso');
      }
    }
  }

  public function mudaEndereco(){
    return "Novo Endereço";
  }
}
