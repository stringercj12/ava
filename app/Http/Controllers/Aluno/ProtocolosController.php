<?php

namespace App\Http\Controllers\Aluno;

use App\Http\Controllers\Controller;
use App\Protocolos;
use App\User;
use Illuminate\Http\Request;

class ProtocolosController extends Controller
{
    public function listaprotocolo()
    {
		// $protocolos = Protocolos::all();
		$user = auth()->user()->id;
		$protocolos = Protocolos::where('user_id', '=', $user)->get();

		$total = $protocolos->count();
		// $dt =  $protocolos->user->titulo;
		// $protocolos = $protocolos->users;
		// dd($protocolos); 
        return view('aluno.protocolos', compact('protocolos', 'total'));
    }
    public function abertura(Request $Request, Protocolos $Protocolos)
    {
        $validar = $Request->titulo;

        // dd($Request->all());
        $result = $Protocolos->create($Request->all());

        if ($result) {
            return redirect()
                ->route('painel.aluno.protocolos')
                ->with('success', 'Protocolo aberto com sucesso');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Falha na abertura do protocolo');
        }

    }
}
