<?php

namespace App\Http\Controllers\Aluno;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cursos;
use App\Alertas;


class BoletimController extends Controller
{
    public function index(){
    	$totalcursos = Cursos::all();
        $Alertas = Alertas::all();
    	return view('aluno.boletim', compact('totalcursos', 'Alertas'));
    }
}
