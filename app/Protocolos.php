<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protocolos extends Model
{
    protected $fillable = [
        'titulo',
        'descricao',
        'anexo1',
        'anexo2',
        'tipo_solicitacao',
       	'user_id',
        'status',
        'nivel_de_dificuldade',
        'data_abertura',
        'data_prevista',
        'data_fechamento',
        'resolucao',
        'anexo_resolucao',
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
