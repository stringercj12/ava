@extends('layouts.app')

@section('title', 'Escola Interativa')
@section('logo', 'Escola Interativa')

@section('content')
<div class="container">
	<section id="breadcrumb">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item active text-secondary"><i class="fas fa-home"></i> Dashboard</li>
			</ol>
		</nav>
	</section>

	<div class="text-center">
		<h2 class="my-3"><i class="fas fa-graduation-cap"></i> Ambiente de Graduação</h2>
	</div>

	<!-- Novidades ou Mensagens importantes -->
	@if(auth()->user()->id === 2)
		<div class="row">
			<div class="col-md-3 mb-5">
				<div class="card">
					<div class="card-header text-center bg-info text-white" style="position: relative;">
						Oferta (Graduação)
						<span class="badge badge-success tag-novo">Novo</span>
					</div>
					<div class="card-body text-center">
						<h1><i class="fab fa-paypal"></i></h1>
						<span>ANÁLISE E DES. DE SISTEMAS Manhã - 2019.2</span>
						<div class="acessarCurso mt-2">
							<a href="https://unicarioca.edu.br/cursos/graduacao/analise-e-desenvolvimento-de-sistemas" target="_blank" class="btn btn-outline-success btn-sm btn-block">Acessar</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif

	<div class="row">
		<div class="col-lg-3 col-12">
			<!-- Card Ações Rápidas-->
			<div class="card">
				<div class="card-header text-center bg-info text-white text-center">
					<h5><i class="fas fa-chart-line"></i> Ações Rápidas</h5>
					<!-- <i class="fas fa-tachometer-alt"></i> -->
				</div>
				<div class="card-body p-0 flex-clumn">
					<a href=" {{ url('http://stringercj13.5gbfree.com') }}" target="_blank" class="btn-acoes-rapidas-home btn-outline-info"> <i class="fas fa-book"></i> Biblioteca</a>
					<a href="#" class="btn-acoes-rapidas-home btn-outline-info"> <i class="fas fa-money-check-alt"></i> Financeiro</a>
					<a href="#" class="btn-acoes-rapidas-home btn-outline-info"> <i class="far fa-calendar-alt"></i> Calendário</a>
					<a href="{{ route('painel.aluno.protocolos') }}" class="btn-acoes-rapidas-home btn-outline-info"> <i class="fas fa-user-shield"></i> Suporte</a>
				</div>
			</div>
		</div>

		<!-- Card Minhas Disciplinas -->

		<div class="col-lg-6 col-12 mt-3 mt-lg-0">
			<div class="card">
				<div class="card-header text-center bg-success text-white">
					<h5><i class="fas fa-th"></i> Minhas Disciplinas</h5> 
				</div>
				<div class="card-body">
						<div class="row justify-content-center align-items-center">
						@if(isset($totalcursos))
							@foreach($totalcursos as $curso)
									<div class="col-12 col-lg-6 my-3">
										<div class="materias">
											<div class="materias-header text-center">
												Segunda-Feira
											</div>
											<div class="materias-body text-center">
												<h1><i class="fab fa-suse"></i></h1>
												<span>{{ $curso->id }} - {{ $curso->name }} 2019/1</span>
	
												
											</div>
											<div class="materias-footer bg-transparent" style="border-top: none">
												
													<button class="btn btn-outline-success btn-block btn-sm">Entrar</button>
												
											</div>
										</div>
									</div>
							@endforeach
						@else
							<div class="col-md-12 mt-3">
								<div class="alert alert-secondary">Você não possui disciplinas matriculadas</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-12 mt-3 mt-lg-0">
			<!-- Card Eventos e lembretes -->

			<div class="card">
				<div class="card-header text-center bg-info text-white text-center">
					<h5><i class="far fa-calendar-alt"></i> Próximos Eventos</h5>
					<!-- <i class="fas fa-tachometer-alt"></i> -->
				</div>
				<div class="card-body p-0">
					<ul class="list-group">
						@foreach($eventos as $evento)
							<li class="list-group-item p-2">
							<a href="#" class="m-0 p-0"data-toggle="modal" data-target="#EventosModal{{$evento->id}}"> {{$evento->name}}</a><br>
								<span class="badge badge-pill badge-primary">{{Date("d/m/Y", strtotime($evento->start))}}</span>
							</li>
						@endforeach
					  	{{-- <li class="list-group-item p-2">
							<a href="#" class="m-0 p-0">  Aulas dos Cursos de Extensão em Letras Números, Física e Bites&Bytes</a><br>
							<span class="badge badge-pill badge-primary">sábado, 11/05, 15:00</span>
				   		</li>
				   		<li class="list-group-item p-2">
							<a href="#" class="m-0 p-0">  Avaliação Institucional (Turmas Presenciais)</a><br>
							<span class="badge badge-pill badge-primary">segunda, 13/05, 08:00</span>
						   </li> --}}
						   
						
				   		<li class="list-group-item p-1 text-center">
							<a href="#" class="">
								Ir para o Calendário
				   			</a>
				   		</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<!-- Outras coisas -->
</div>



@foreach($eventos as $evento)
<!-- Modal Calendário -->
<div class="modal fade" id="EventosModal{{ $evento->id }}" tabindex="-1" role="dialog" aria-labelledby="EventosModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $evento->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			@switch($evento->status)
				@case('Confirmado')
					<div class="alert alert-success">
						<strong><i class="fas fa-check-circle"></i> {{ $evento->status }}</strong>
					</div>
				@break
				@case('Pendente')
					<div class="alert alert-warning">
						<strong><i class="fas fa-exclamation-circle"></i> {{ $evento->status }}</strong>
					</div>
				@break
				@case('Cancelado')
					<div class="alert alert-danger">
						<strong><i class="fas fa-times-circle"></i> {{ $evento->status }}</strong>
					</div>
				@break
			  @default
		  @endswitch
        <div>
        	{{ $evento->detalhes }}
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
@stop

@section('footer')
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="logo">
						<h5>Desenvolvido por JP - Produções de Softwares e Planilhas</h5>
					</div>
				</div>
				<div class="col-md-9">
					outras coisas
				</div>
			</div>
		</div>
	</div>
@stop