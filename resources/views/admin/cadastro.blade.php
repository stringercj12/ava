@extends('adminlte::page')

@section('title', 'Cadastro de Usuários')

@section('content_header')
    <h1>Novo Usuário</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Cadastro</li>
    </ol>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <a class="btn btn-info" href="{{route('admin.home')}}"> <i class="fa fa-reply"></i> Voltar</a>
        </div>
        @if(session('success'))
            {{ session('success') }}
        @endif
        <div class="box-body">
            <p>Qual usuário deseja cadastrar ?</p>

            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#cad-aluno" class="btn text-secondary" data-toggle="tab"
                                      aria-expanded="false"><i class="fa fa-graduation-cap"></i> Aluno</a></li>
                <li><a href="#cad-professor" class="btn text-secondary" data-toggle="tab" aria-expanded="false"><i
                            class="fa fa-users"></i> Professor</a></li>
                <li><a href="#cad-outro" class="btn text-secondary" data-toggle="tab" aria-expanded="false"><i
                            class="fa fa-spin fa-spinner"></i> Gerar boletos</a></li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane active" id="cad-aluno">
                    Alunos
                    <form method="post" action="{{ route('url_admin_cadastro_create') }}">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label>Nome Completo</label>
                            <input type="text" name="nome" class="form-control" placeholder="Nome Completo">
                        </div>

                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="text" name="email" class="form-control" placeholder="E-mail">
                        </div>

                        <div class="form-group">
                            <label>CPF</label>
                            <input type="text" name="cpf" class="form-control" placeholder="CPF">
                        </div>

                        <div class="form-group">
                            <label>RG</label>
                            <input type="text" name="rg" class="form-control" placeholder="RG">
                        </div>

                        <div class="form-group">
                            <label>Sexo</label>
                            <div class="form-group">
                                <input type="radio" name="sexo" class="form-radios" value="Masculino"> Masculino

                                <input type="radio" name="sexo" class="form-radios" value="Feminino"> Feminino
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nascimento</label>
                            <input type="text" name="nascimento" class="form-control" placeholder="Nascimento">
                        </div>

                        <div class="form-group">
                            <label>Telefones</label>
                            <div class="form-group">
                                <input type="text" name="celular" class="form-control"
                                       placeholder="Ex: (19) 93386-1239 ">
                            </div>
                            <div class="form-group">
                                <input type="text" name="residencial" class="form-control"
                                       placeholder="Ex: (19) 5446-3585 ">
                            </div>
                        </div>

                        <ul class="nav nav-tabs">
                            <li class="text-secondary active"><a href="#endereco" data-toggle="tab"
                                                                 class="btn text-secondary"><i
                                        class="fa fa-location-arrow"></i> Endereço</a></li>
                            <li><a href="#escolha-curso" data-toggle="tab" class="btn text-orange"><i
                                        class="fa fa-graduation-cap"></i> Escolha Curso</a></li>
                            <li><a href="#obs" data-toggle="tab" class="btn text-orange"><i
                                        class="fa fa-newspaper-o"></i> Observações</a></li>
                            <li><a href="#finalizacao" data-toggle="tab" class="btn text-orange"><i
                                        class="fa fa-vcard"></i> Finalização</a></li>
                            <li><a href="#gerar-boleto" data-toggle="tab" class="btn text-orange"><i
                                        class="fa fa-file-pdf-o text-dark"></i> Gerar Boleto</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- Tab Endereço -->
                            <div class="tab-pane active" id="endereco">
                                <div class="form-group">
                                    <label>Rua</label>
                                    <input type="text" name="rua" class="form-control" placeholder="Rua">
                                </div>

                                <div class="form-group">
                                    <label>Número</label>
                                    <input type="text" name="numero" class="form-control" placeholder="Número">
                                </div>

                                <div class="form-group">
                                    <label>CEP</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="cep" placeholder="CEP">

                                        <div class="input-group-addon" style="cursor:pointer">
                                            <span id="BuscaCep"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" class="form-control" placeholder="Bairro">
                                </div>

                                <div class="form-group">
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" class="form-control" placeholder="Cidade">
                                </div>

                                <div class="form-group">
                                    <label>Estado</label>
                                    <select class="form-control">
                                        <option>-- Escolha o uma opção --</option>
                                        <option value="RJ">Rio de Janeiro</option>
                                        <option value="SP">São Paulo</option>
                                        <option value="MG">Minas Gerais</option>
                                        <option value="ES">Espirito Santos</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Tab Escolha do curso -->
                            <div class="tab-pane" id="escolha-curso">
                                <label>Escolha de Cursos</label>
                                <select class="form-control">
                                    <option>--Escolha o Curso</option>
                                    <option value="">Ánalise e Desenvolvimento de Sistemas</option>
                                    <option value="">Administração</option>
                                    <option value="">Engenharia</option>
                                    <option value="">Biologia</option>
                                </select>

                                <p>
                                    <strong>Curso Escolhido:</strong> <span>Administração</span>
                                </p>

                                <div class="form-group">
                                    <button class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Ver Detalhes</button>
                                </div>


                                <div class="form-group">
                                    <label>Turno</label>
                                    <div class="form-group">
                                        <div class="col-4">
                                            <input type="radio" name="turno" class="form-radios" value="Manhã">
                                            <span>Manhã</span>
                                        </div>
                                        <div class="col-4">
                                            <input type="radio" name="turno" class="form-radios" value="Tarde">
                                            <span>Tarde</span>
                                        </div>
                                        <div class="col-4">
                                            <input type="radio" name="turno" class="form-radios" value="Noite">
                                            <span>Noite</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label>Escolha o Vencimento</label>
                                    <select class="form-control">
                                        <option>-- Escolha o Vencimento --</option>
                                        <option value="05">05</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                        <option value="25">25</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="form-group icheckbox_flat-red">
                                        <label>Tipo de Fatura</label><br>
                                        <input type="radio" name="tipoFatura" class="flat-red" value="Digital"> <span>Digital</span>

                                        <input type="radio" name="tipoFatura" class="form-radios" value="Correio">
                                        <span>Correio</span> <br>
                                    </div>
                                    <div class="form-group">
                                        E-mail Boleto Digital
                                        <input type="text" placeholder="E-mail Digital" name="EmailFaturaDigital"
                                               class="form-control">
                                        <small><i class="fa fa-exclamation-circle text-red"></i> Informe um e-mail
                                            válido pois será enviada o boleto para o mesmo.</small>
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" data-inputmask="'alias': 'ip'"
                                               data-mask="">
                                    </div>
                                </div>

                            </div>
                            <!-- Tab Observações -->
                            <div class="tab-pane" id="obs">
                                <p>Observações</p>

                                <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Esta
                                            &eacute; minha textarea para ser substitu&iacute;da pelo
                                            CKEditor.</font></font></p>
                            </div>
                            <!-- Tab Gerar Boleto -->
                            <div class="tab-pane" id="gerar-boleto">
                                <p>Gerar Boleto</p>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Salvar
                            </button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="cad-professor">
                    Professor
                    <form method="post" action="">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <input type="text" name="nome" class="form-control" placeholder="Nome Completo">
                        </div>

                        <div class="form-group">
                            <input type="text" name="email" class="form-control" placeholder="E-mail">
                        </div>

                        <div class="form-group">
                            <input type="text" name="cpf" class="form-control" placeholder="CPF">
                        </div>

                        <div class="form-group">
                            <input type="radio" name="sexo" class="form-control" value="Masculino"> Masculino
                            <input type="radio" name="sexo" class="form-control" value="Feminino"> Feminino
                        </div>

                        <div class="form-group">
                            <input type="text" name="nascimento" class="form-control" placeholder="Nascimento">
                        </div>

                        <div class="form-group">
                            <input type="text" name="endereco" class="form-control" placeholder="Endereço">
                        </div>

                        <div class="form-group">
                            <input type="text" name="numero" class="form-control" placeholder="Número">
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Salvar
                            </button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="cad-outro">Outros usuários</div>
            </div>

        <!-- <form method="post" action="">
    			{!! csrf_field() !!}
            <div class="form-group">
                <input type="text" name="nome" class="form-control" placeholder="Nome Completo">
            </div>

            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="E-mail">
            </div>

            <div class="form-group">
                <input type="text" name="cpf" class="form-control" placeholder="CPF">
            </div>

            <div class="form-group">
                <input type="radio" name="sexo" class="form-control" value="Masculino"> Masculino
                <input type="radio" name="sexo" class="form-control" value="Feminino"> Feminino
            </div>

            <div class="form-group">
                <input type="text" name="nascimento" class="form-control" placeholder="Nascimento">
            </div>

            <div class="form-group">
                <input type="text" name="endereco" class="form-control" placeholder="Endereço">
            </div>

            <div class="form-group">
                <input type="text" name="numero" class="form-control" placeholder="Número">
            </div>
        </form> -->
        </div>
    </div>
@stop
