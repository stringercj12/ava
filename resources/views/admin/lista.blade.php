@extends('adminlte::page')

@section('title', 'Gerenciar Usuários')

@section('content_header')
    <h1>Gerenciar Usuários</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Lista Usuários</li>
    </ol>
@stop

@section('content')

    <div class="box">
        <div class="box-header">
            <h3>Lista de Usuários
                <div style="float:right;">
                    <a href="{{ route('url_admin_cadastro') }}" class="btn btn-success"><i
                            class="fa fa-plus-circle"></i> Novo Usuário</a>
                </div>
            </h3>
        </div>
        <div class="box-body">
            @if (session('acao'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <div class="table-responsive">
                <table id="tabela" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="info text-center text-uppercase">ID</th>
                        <th class="info text-center text-uppercase">Nome</th>
                        <th class="info text-center text-uppercase">E-mail</th>
                        <th class="info text-center text-uppercase">Cadastro</th>
                        <th class="info text-center text-uppercase">Usuário</th>
                        <th class="info text-center text-uppercase">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($nomes as $nome)
                        <tr class="text-center">
                            <td>{{ $nome->id }}</td>
                            <td>{{ $nome->name }}</td>
                            <td>{{ $nome->email }}</td>
                            <td>{{ $nome->created_at }}</td>
                            <td>{{ $nome->tipoUsuario }}</td>
                            <td class="dt-nowrap">
                                <a href="#" class="btn btn-default text-green btn-xs tg"><i class="fa fa-edit"></i></a>
                                <a href="#" class="btn btn-default text-red btn-xs tr del" data-toggle="modal"
                                   data-target="#delModal"><i class="fa fa-trash"></i></a>
                                <a href="#" data-toggle="dropdown"
                                   class="dropdown-toggle btn btn-default text-orange btn-xs to"><i
                                        class="fa fa-ellipsis-h"></i></a>
                                <div class="dropdown">
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item"><i class="fa fa-lock"></i> Mudar Senha</a></li>
                                        <li><a class="dropdown-item"><i class="fa fa-ban"></i> Bloquear</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Modal de confirmação de trash -->
    <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Deseja deletar o usuário ?</h4>
                </div>
                <div class="modal-body text-center">
                    Lembre-se com isso será apagada todas as informações e solicitações pendentes ou em andamento desse
                    usuário.
                </div>
                <div class="modal-footer">
                    <div class="text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-bash"></i> Não
                        </button>
                        <a href="/dashboard/lista" onclick="alert('Excluido com sucesso')" class="btn btn-success"><i
                                class="fa fa-check"></i> Sim</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('.dropdown').dropdown();
        $('#delModal').modal();
    </script>
    <style type="text/css">
        .tg:hover {
            background-color: #00a65a;
            color: #ffffff !important;
            border: 1px solid #00a65a;
        }

        .tr:hover {
            background-color: #dd4b39;
            color: #ffffff !important;
            border: 1px solid #dd4b39;
        }

        .to:hover {
            background-color: #ff851b;
            color: #ffffff !important;
            border: 1px solid #ff851b;
        }
    </style>


@stop
