@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
    <h1>Meu Perfil</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home')}}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('admin.settings')}}"><i class="fa fa-cog"></i> Configurações</a></li>
        <li class="active">Perfil</li>
    </ol>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <a href="{{ route('admin.home')}}" class="btn btn-info"><i class="fa fa-reply"></i> Voltar</a>
            <a  href="#" class="btn btn-default"><i class="fa fa-edit"></i> Editar</a>
        </div>
        <div class="box-body">
            <div class="text-center">
                <label>Foto de Perfil</label>
                <div class="justify-content-item">
                    @if( auth()->user()->image)
                    <img src="{{ auth()->user()->image }}" class="img-circle">
                    @else
                        <div class="img-circle" style="width: 160px; height: 160px; display: inline-block; background-color: #e4e2e2; border: 1px dotted #777;">
                            <label for="upload-img"><i style="font-size: 40px;padding-top: 40px; cursor: pointer;" class="fa fa-camera"></i></label>
                            <input type="file" name="upload-img" class="sr-only" id="upload-img">
                            <div class="">Arraste uma imagem ou clique na camera para alterar</div>
                        </div>
                    @endif
                </div>
            </div>
            <br>
            <div>
                <form method="post" action="">
                    <!-- dados do perfil para alterar e visualizar -->
                    <div class="form-group">
                        <label>Nome Completo</label>
                        <input type="text" name="nome" placeholder="Nome Completo" value="{{ auth()->user()->name }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" disabled name="email" placeholder="E-mail de Contato" value="{{ auth()->user()->email }}">
                        <small><i class="fa fa-exclamation-circle"></i> Para alterar o e-mail entre em contato conosco através da <a href="">Central de Ajudas.</a></small>
                    </div>

                    <div class="form-group">
                        <label>Usuário criado em</label>
                        <input type="text" name="nome" placeholder="Nome Completo" value="{{ auth()->user()->created_at }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <p><label>Senha</label> Para alterar a Senha vá até o painel de senhas.</p>
                        <p> <a href="{{ route('admin.settings.senhas')}}" class="btn btn-sm btn-info"><i class="fa fa-lock"></i> Acessar o painel</a></p>
                    </div>

                    <div class="form-group">
                        <label>Endereço</label>
                        <input type="text" class="form-control" name="endereco" placeholder="Endereço" value="Rua Flamingo de Ana Gonzaga">
                    </div>  

                    <div class="form-group">
                        <label>Número</label>
                        <input type="text" class="form-control" name="numero" placeholder="Número" value="SN">
                    </div>
                    
                    <div class="form-group">
                        <label>Complemento</label>
                        <input type="text" class="form-control" name="complemento" placeholder="Complemento" value="Antiga Rua 14 Lt 31 Qd 18">
                    </div> 

                    <div class="form-group">
                        <label>CEP</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="cep" placeholder="CEP" value="23059-492">

                            <div class="input-group-addon" style="cursor:pointer">
                                <span id="BuscaCep"><i class="fa fa-search"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Bairro</label>
                        <input type="text" class="form-control" name="bairro" placeholder="Bairro" value="Inhoaiba">
                    </div>  

                    <div class="form-group">
                        <label>Cidade</label>
                        <input type="text" class="form-control" name="cidade" placeholder="Cidade" value="Rio de Janeiro">
                    </div> 

                    <div class="form-group">
                        <label>Estado</label>
                        <input type="text" class="form-control" name="estado" placeholder="Estado" value="Rio de Janeiro">
                    </div>

                    <!-- Botão salvar -->
                    <div class="text-right">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                    </div>
                </form>
            </div>

            <p>e demais dados a ser criado</p>
        </div>
    </div>
@stop