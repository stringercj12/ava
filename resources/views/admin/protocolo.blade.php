@extends('layouts.app')

@section('title', 'Escola Interativa')
@section('logo', 'Escola Interativa')

@section('content')
<div class="container">
    <section id="breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item text-secondary"><a href="{{  route('admin.home.teste') }}"><i class="fas fa-home"></i> Dashboard</a></li>
                <li class="breadcrumb-item active text-secondary"><i class="fas fa-home"></i> Protocolos</li>
            </ol>
        </nav>
    </section>

    <div class="card">
        <div class="card-header">
            <span class="h5">Meus Processos</span>
            <span class="filtro-protocolo float-right d-flex">
                <form class="form-row mr-2">
                    <select class="form-control">
                        <option hidden>Filtros</option>
                        <option value="Aberto">Aberto</option>
                        <option value="Andamento">Andamento</option>
                        <option value="Fechado">Fechado</option>
                    </select>
                </form>
                <button class="btn btn-outline-success btn-sm ml-2"><i class="fas fa-plus"></i> Novo</button>
            </span>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <h4>Protocolos</h4>
                    <div class="qtd-protocolos">
                        <h6>Total: 4 Tickets</h6>
                    </div>
                    <div class="accordion" id="accordionExample">
                        @for($i = 2; $i <= 6; $i++)
                              <div class="card">
                                <div class="card-header" id="heading{{$i}}">
                                  <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                                      {{$i * 427422  }} - {{$titulo}}
                                    </button>
                                  </h2>
                                </div>

                                <div id="collapse{{$i}}" class="collapse" aria-labelledby="heading{{$i}}" data-parent="#accordionExample">
                                  <div class="card-body">
                                      <table class="table">
                                        <thead>
                                            <tr class="text-center">
												<th>Solicitante</th>
												<th>Data</th>
												<th>Observação</th>
												<th>Situação</th>
												<th>Ações</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
												<td>Jefferson Ferreira</td>
												<td>23/05/2018</td>
                                                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>

                                                @if($i % 2 == 0)
													<td>Aberto</td>
                                                @else
													<td>Fechado</td>
                                                @endif
                                                <td class="text-center"><a href="#"><i class="fas fa-clone"></i></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                          @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop