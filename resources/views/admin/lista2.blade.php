@extends('adminlte::page')

@section('title', 'Gerenciar Usuários')

@section('content_header')
    <h1>Gerenciar Usuários</h1>
    <ol class="breadcrumb">
    	<li><a href="{{ route('admin.home') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li class="active">Lista Usuários</li>
    </ol>
@stop

@section('content')
<style type="text/css">
	#tabela_previous::before{
		content: '\f053'
	}
	#tabela_next::before{
		content: '\f054'
	}
	#tabela_filter,#tabela_length{
		display: block;
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    font-size: 14px;
	    line-height: 1.42857143;
	    color: #555;
	    background-color: #fff;
	    background-image: none;
	    border: 1px solid #ccc;
	    border-radius: 4px;
	    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	}
</style>
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Data Table With Full Features</h3>
	</div> <!-- /.box-header -->
	<div class="box-body">
	  <table id="tabela" class="table table-bordered table-striped">
	    <thead>
		    <tr>
		      <th>#</th>
		      <th>Nome</th>
		    </tr>
	    </thead>
	   	<tbody>
	   		@foreach($nomes as $nome)
	   		<tr>
	   			<td>{{ $nome->id }}</td>
	   			<td>{{ $nome->name }}</td>
	   		</tr>
	   		@endforeach
	   	</tbody>
	  </table>
	</div> <!-- box body -->
</div> <!-- box -->
  @stop