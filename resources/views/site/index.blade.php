<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>As Aventuras de Jackie Chan</title>
	<link rel="stylesheet" type="text/css" href="http://localhost/jc/materialize/css/materialize.css">
	 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	 <link rel="shortcun icon" type="image/x-icon" href="http://localhost/jc/images/s_logo1.png">
	 <style type="text/css">
		strong{
			color: #000 !important;
		}
        .slide{
			max-width:100%;
            overflow:auto;
        }
        .slide li{
            height:300px;
            text-align:center;
        }
     </style>
     <link rel="stylesheet" type="text/css" href="http://localhost/jc/slick/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="http://localhost/jc/slick/slick/slick-theme.css"/>
</head>
<body>
	<nav class="red">
		<div class="nav-wrapper">
			<div class="container">
			<a href="http://localhost/jc/" class="brand-logo">
			<img title="Jackie Chan Adventures" alt="Jackie Chan Adventures" src="http://localhost/jc/images/outras/s_logo3.png" style="margin-top: -10px;"></a>
			</div>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
			<li><a href="http://localhost/jc/" class="bor breadcrumb">Home</a></li>
			<li><a href="historia.php" class="bor breadcrumb">Historia</a></li>
			<li><a href="equipe.html" class="bor breadcrumb">Equipe</a></li>
			<li><a href="contato.html" class="bor breadcrumb">Contato</a></li>
			<li ><a href="{{ route('admin.home') }}" data-activates="last_modal" class="right bor"><i class="material-icons left">perm_identity</i> Login</a></li>
		</ul>
		</div>
	</nav>

<!-- slide -->

<ul class="slide">
    <li><img src="http://localhost/jc/images/jackie22.jpg" alt=""></li>
    <li><img src="http://localhost/jc/images/jackie.jpg" alt=""></li>
    <li><img src="http://localhost/jc/images/caixa-pan-ku.jpg" alt=""></li>
    <li><img src="http://localhost/jc/images/Talismas.jpg" alt=""></li>
    <li><img src="http://localhost/jc/images/ja.jpg" alt=""></li>
    <li><img src="http://localhost/jc/images/01.jpg" alt=""></li>
</ul>

<!-- fim slide -->

	<!-- <div class="row"> 	
		<div id="slide" class="s12">
		    Slide aqui 
		</div>
	</div> -->
	<div class="row">
		<div class="row"></div>
		<div class="container s12 m6">
			<h5>As Aventuras de Jackie Chan</h5>
			<p class="black-text flow-text">Jackie Chan Adventures nome original, treaduzido no Brasil para As Aventuras de Jackie Chan. É uma série de desenho animado estadunidense, que foi exibida originalmente pela Kids' WB entre 9 de setembro de 2000 e 7 de julho de 2005, totalizando 95 episódios em 5 temporadas, ainda nos Estados Unidos a série foi exibida pelo Cartoon Network e Disney XD . Em Portugal a série foi emitida na TVI e agora está na SIC K. No Brasil foi exibida pela Rede Globo e pelo Cartoon Network. A série descreve uma forma fictícia dos filmes de Jackie Chan. Muitos episódios contêm referências aos trabalhos atuais de Chan. A série é composta por muita luta, inspirada no próprio Jackie Chan e também ficção, já que o personagem "Tio" utiliza magia em muitos casos.</p>			 
			<p>O character design da série ficou a cargo de Jeff Matsuda.</p>
		</div>
	</div>

	<section class="row container">
		<article class="col s4 l4">

			<div class="card">

			<h5 class="center-align red-text ">1º Temporada</h5>

			<div class="card-image">
				<img src="http://localhost/jc/images/talismans/Talisman1.png">
			</div>

			<div class="card-content">
				<span class="card-title activator center-align">Os Doze Talismãs <i class="material-icons right" title="Saiba Mais">arrow_drop_up</i></span>
				<p class="center-align">O arqueólogo Jackie Chan em uma de suas pesquisas encontra um escudo...</p>
				<br>
				<a href="http://localhost/jc/temporadas.html/#primeiraTemporada" class="right">Assistir a todos</a>

			</div>
			<div class="card-reveal">
				<span class="card-title">Os Doze Talismãs<i class="material-icons right">arrow_drop_down</i></span>
				<p class="center-align">O arqueólogo Jackie Chan em uma de suas pesquisas encontra um escudo com um talismã e sua vida muda completamente. Após reencontrar seu amigo Capitão Augustus Black, ele tem de deter a Mão Negra (organização secreta do mal), encontrar e proteger 12 talismãs para não reviver Shendu, e conta com a ajuda de sua sobrinha Jade, Tio, Capitão Black e a Sessão 13 para deter as forças das trevas.</p>

				<a href="#">Assistir a 1ª Temporada.</a>
			</div>
		</article>

		<article class="col s4 l4">
			<div class="card">
			<h5 class="center-align red-text">2º Temporada</h5>
			<div class="card-image">
				<img src="http://localhost/jc/images/demonios/teste4.jpg">
			</div>
			<div class="card-content">
				<span class="card-title activator">Os Portais dos Demônios<i class="material-icons right">arrow_drop_up</i></span>
				<p class="center-align">Depois da suposta destruição de Shendu, Jackie acha que está livre das forças do mal...</p>
			<br>
				<a href="http://localhost/jc/temporadas.html/#segundaTemporada" class="right">Assistir a todos</a>
			</div>

			<div class="card-reveal">
				<span class="card-title activator">Os Portais dos Demônios - Artefatos Mágicos e suas Maldições<i class="material-icons right">arrow_drop_down</i></span>
				<p class="center-align">
				Depois da suposta destruição de Shendu, Jackie acha que está livre das forças do mal, mas... não é bem assim, os pais de Jade decidem deixá-la na casa de Jackie por mais um ano e desta vez eles têm de pegar a Caixa Panku e impedir Shendu (que está no corpo de Valmont) de libertar os seus 7 irmãos demônios e se vingar, juntos eles ainda vão passar por muitas aventuras. No final dessa temporada existem 20 episódios extras, fora dos capítulos sequenciais da temporada regular.</p>
			</div>
		</div>
		</article>

		<article class=" col s4 l4">
			<div class="card">
			<h5 class="center-align red-text">3º Temporada</h5>
			<div class="card-image">
				<img src="http://localhost/jc/images/animais/espiritos-animais.jpg">
			</div>

			<div class="card-content">
				<span class="card-title activator">Os Espíritos dos Talismãs<i class="material-icons right">arrow_drop_up</i></span>
				<p class="center-align">Mas uma vez Jackie e sua turma tem de sair a procura de 12 animais que absorveram ...</p>
			<br>
				<a href="http://localhost/jc/temporadas.html/#terceiraTemporada" class="right">Assistir a todos</a>
			</div>

			<div class="card-reveal">
				<span class="card-title activator">Os Espíritos dos Talismãs<i class="material-icons right">arrow_drop_down</i></span>
				<p class="center-align">
				 Agora Jackie e sua turma tem de sair a procura de 12 animais que absorveram os poderes dos talismãs e mais uma vez impedir que a Mão Negra coloque as mãos nesses poderosos animais que podem ser muito perigosos se cairem em mãos erradas, agora não de Valmont, mais sim de um mago do mal chamado Daolon Wong.</p>
			</div>
		</div>
		</article>
	</section>
	<div class="row"></div>
	<div class="row container">
		<p>Caso prefira assistir em sequencia as temporadas clique no botão abaixo.</p>
		<div class="card-panel small">
			<a href="temporadas.html" class="btn">Ver Todas as Temporadas</a>
			<a href="#" class="btn green right">Enviar Link por e-mail</a>
		</div>		
	</div>

<!-- chat -->
	
<div class="fixed-action-btn">
  <a class="btn-floating btn-large red" title="Chat">
    <i class="large material-icons">message</i>
  </a>
  <ul>
    <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
    <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
    <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
    <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
  </ul>
</div>

<!-- fim chat -->
      

<!-- rodape -->
	<div class="row red">
		<div class="row container s4">
			<div class="row"></div>
		<div class="card red center-align">
			<span class=" white-text"> Assine agora nossa newslleter e Fique por dentro das novidades e dicas do site AnimeFat</span>
		</div>
		<form action="newsletter.php" method="post" class="">
			<input style="color: white;" type="email" class="" name="email" placeholder="Seu e-mail..">
			<button class="btn white waves-effect waves-teal red-text" name="assinar" type="submit">Enviar</button>
		</form>
	</div>
		<div class="row container center-align">
			<a href="" class="waves-effect white-text left">Copyright © Animefat 2017-2018. Os melhores animes aqui.</a>
			<a href="" class="waves-effect white-text right">Criado por By Jefferson Ferreira</a>
		</div>
	</div>

<script type="text/javascript" src="http://localhost/jc/materialize/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="http://localhost/jc/materialize/js/materialize.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="http://localhost/jc/slick/slick/slick.min.js"></script>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.fixed-action-btn').floatingActionButton();
  });

//   slide de imagens
$('.slide').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  nextArrow: '<button type="button" class="slick-next">Next</button>',
  prevArrow: '<button type="button" class="slick-prev">Previous</button>',
  autoplay: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>
</body>
</html><!-- 




		<div class="banner">
			<img src="images/chan.png" width="135" title="As Aventuras de Jackie Chan">
		</div>
    
	<main class="corpo">

		<article>

			<p>As Aventuras de Jackie Chan (Jackie Chan Adventures no original) é uma série de desenho animado estadunidense, que foi exibida originalmente pela <a href="">Kids' WB </a>entre 9 de setembro de 2000 e 7 de julho de 2005, totalizando 95 episódios em 5 temporadas, ainda nos Estados Unidos a série foi exibida pelo <a href="http://www.cartoonnetwork.com.br/">Cartoon Network</a> e <a href="http://disneyxd.disney.com.br/">Disney XD</a>.</p>
			<p>Em Portugal a série foi emitida na <b>TVI </b>e agora está na <b> SIC K</b>.</p>
			<p>No Brasil foi exibida pela <a href="http://redeglobo.globo.com/" title="Rede Globo
			Globo Comunicação e Participações S.A.
			Rede Globo é uma rede de televisão comercial 
			aberta brasileira com sede na cidade do Rio de Janeiro."><b>Rede Globo</b></a>  e pelo Cartoon Network </p>			 
			<p>A série descreve uma forma fictícia dos filmes de Jackie Chan. Muitos episódios contêm referências aos trabalhos atuais de Chan. A série é composta por muita luta, inspirada no próprio Jackie Chan e também ficção, já que o personagem "Tio" utiliza magia em muitos casos.</p>			 
			<p>O character design da série ficou a cargo de Jeff Matsuda.</p>


		</article>


    <div class="trailer">
    		<p>Abertura do Anime 2º Temporada, <a href="videos.html">veja aqui todas as aberturas.</a></p>
        <video controls="controls" poster="videos/frente-video.jpg"> <source src="videos/2 Abertura as aventuras de jackie chan.webm" type="video/webm"/></video>
    </div>
        
</main>


		<!-- NEWSLETTER -->

		<!-- RODAPÉ -->
		<!-- <footer class="rodape container bg-gradient">
			<div class="social-icons">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-google"></i></a>
				<a href="#"><i class="fa fa-instagram"></i></a>
				<a href="#"><i class="fa fa-envelope"></i></a>
			</div>
			<p class="copyright">
				 Copyright © Animefat 2017 Os melhores animes aqui.
			</p>
		</footer> -->

<!-- </body>

</html> -->