@extends('layouts.app')

@section('title', 'Escola Interativa')
@section('logo', 'Escola Interativa')

@section('content')
    <div class="container">

      @if(session('success'))
    		<div class="alert alert-success">
    			{{ session('success')}}
    		</div>
    	@endif
    	@if(session('error'))
    		<div class="alert alert-danger">
    			{{ session('error')}}
    		</div>
    	@endif


    	<div class="card">
	        <div class="card-header text-center border-0 mb-2">
	            <h1>Meu Perfil</h1>
	        </div>
    		<div class="row mx-2">
    			<div class="col-md-8">
			        <div class="flex-column" style="margin-bottom: 8px;border-bottom: 1px solid #ccc">
			            <div class="menu-modal">
			                <div class="img-fundo" style="background-image: url('{{ auth()->user()->temafundo }}');">
			                    <ul class="menu-modal-perfil p-0 text-center" style="list-style: none;">
			                        <li class="menu-img nav-item">
			                            <a href="#">
			                                <img src="{{ auth()->user()->image }}" alt="img Perfil" style="width: 64px;height: 64px;border-radius: 50%;">
			                            </a>
			                        </li>
			                        <li class="menu-text-name nav-item">
			                            <a href="#" class="menu-modal-link-btn menu-modal-link-perfil"><i class="fas fa-user"></i> {{ auth()->user()->name }}</a>
			                        </li>
			                        <li class="menu-text-email nav-item">
			                            <a href="#" class="menu-modal-link-btn menu-modal-link-perfil"><i class="fas fa-envelope"></i> {{ auth()->user()->email }}</a>
			                        </li>
			                    </ul>
			                </div>
			            </div>
			      </div>
			      <div class="card-body">
			      	<ul class="nav nav-tabs" id="myTab" role="tablist">
					  <li class="nav-item">
					    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dados Pessoais</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Endereço</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Matrícula</a>
					  </li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
							  	<div class="form-group">
			                        <label>Nome Completo</label>
			                        <input type="text" name="nome" placeholder="Nome Completo" value="{{ auth()->user()->name }}" class="form-control" disabled="">
			                    </div>

			                    <div class="form-group">
			                        <label>E-mail</label>
			                        <input type="text" class="form-control" disabled="" name="email" placeholder="E-mail de Contato" value="{{ auth()->user()->email }}">
			                        </div>

			                    <div class="form-group">
			                        <label>Usuário criado em</label>
			                        <input type="text" name="nome" placeholder="Criado em" value="{{ auth()->user()->created_at }}" class="form-control" disabled="">
			                    </div>

			                    <div class="form-group">
			                        <p>Para alterar a Senha vá até o painel de senhas.</p>
			                        <!--{{-- <p> <a href="{{ route('painel.config.senha')}}" class="btn btn-sm btn-info"><i class="fa fa-lock"></i> Acessar o painel</a></p> --}} -->
			                    </div>
              						</div>
              						<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              							<div class="row">
              								<div class="col-md-9">
              									<div class="form-group">
				                              <label>Rua <span class="asterisco">*</span></label>
				                              <input type="text" class="form-control" disabled="" name="endereco" placeholder="Rua, AV, Travessa..." value="Rua Flamingo de Ana Gonzaga">
    				                    </div>
    								          </div>

    			                    <div class="col-md-3">
    			                    	<div class="form-group">
    				                        <label>Número</label>
    				                        <input type="text" class="form-control" disabled="" name="numero" placeholder="Número" value="SN">
    				                    </div>
    			                    </div>
							              </div>

            							<div class="row">
            								<div class="col-md-9">
            									<div class="form-group">
  				                        <label>Complemento</label>
  				                        <input type="text" class="form-control" disabled="" name="complemento" placeholder="Complemento" value="Antiga Rua 14 Lt 31 Qd 18">
  				                    </div>
								            </div>
  			                    <div class="col-md-3">
  				                    <div class="form-group">
  				                        <label>CEP</label>
  				                        <input type="text" class="form-control" disabled="" name="cep" placeholder="CEP" value="23059-492">
  				                    </div>
  			                    </div>
							            </div>

		                    <div class="row">
		                    	<div class="col-md-4">
		                    		<div class="form-group">
				                        <label>Bairro</label>
				                        <input type="text" class="form-control" disabled="" name="bairro" placeholder="Bairro" value="Inhoaiba">
				                    </div>
		                    	</div>
		                    	<div class="col-md-4">
		                    		<div class="form-group">
				                        <label>Cidade</label>
				                        <input type="text" class="form-control" disabled="" name="cidade" placeholder="Cidade" value="Rio de Janeiro">
				                    </div>
		                    	</div>
		                    	<div class="col-md-4">
		                    		<div class="form-group">
				                        <label>Estado</label>
				                        <input type="text" class="form-control" disabled="" name="estado" placeholder="Estado" value="Rio de Janeiro">
				                    </div>
		                    	</div>
		                    </div>
						</div>
						<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
							<p>
								<div class="alert alert-warning">
									<p><i class="fas fa-exclamation"></i> Caso o usuário logado seja aluno abrira esta aba com informações do curso em andamento</p>
								</div>
							</p>
						</div>
					</div>
			      </div>
			    </div>
			    <div class="col-md-4">
		    		<div class="card">
		    			<div class="card-header text-center">
		    				<h3><i class="fas fa-edit"></i> Editar Perfil</h3>
		    			</div>
		    			<div class="card-body">
		    				<div class="list-group">
		    					<a href="#" class="list-group-item btn btn-outline-secondary text-left btn-sm" class="btn btn-primary" data-toggle="modal" data-target="#AlteraImagePerfil">
		    						<i class="fas fa-camera"></i> Imagem de Perfil
		    					</a>
		    					<a href="#" class="list-group-item btn btn-outline-secondary text-left btn-sm" class="btn btn-primary" data-toggle="modal" data-target="#AlteraImageFundo">
		    						<i class="fas fa-image"></i> Tema de Fundo
		    					</a>
		    					<a href="#" class="list-group-item btn btn-outline-secondary text-left btn-sm" class="btn btn-primary" data-toggle="modal" data-target="#AlterarDadosPessoais">
		    						<i class="fas fa-user-edit"></i> Dados Pessoais
		    					</a>
		    					<a href="#" class="list-group-item btn btn-outline-secondary text-left btn-sm" class="btn btn-primary" data-toggle="modal" data-target="#AlteraEndereco">
		    						<i class="fas fa-street-view"></i> Endereço
		    					</a>
		    					<a href="#" class="list-group-item btn btn-outline-secondary text-left btn-sm" class="btn btn-primary" data-toggle="modal" data-target="#AlteraSenha">
		    						<i class="fas fa-lock"></i> Nova Senha
		    					</a>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
    		</div>

	      <div class="card-footer">
          <small><i class="fa fa-exclamation-circle"></i> Caso precise de ajuda entre em contato conosco através da <a href="">Central de Ajudas.</a></small>
      </div>
    	</div>
    </div>


<!-- Modais do menu -->

<!-- 1 Modal de trocar imagem perfil -->
<div class="modal fade" id="AlteraImagePerfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alterar Imagem de Perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
        	<strong><i class="fas fa-exclamation-circle"></i> Tamanho da imagem deve ser superio a 4000X6000</strong>
        </div>
        <div class="trocaImage">
        	<form method="post" action="{{ route('painel.aluno.config.trocaImagePerfil')}}" enctype="multipart/form-data">
        		{!! csrf_field() !!}
        		<div class="form-group">
        			<input class="form-file" type="file" name="trocaImagePerfil">
        		</div>
            <div class="float-right mt-5">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
              <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- 2 Modal de trocar imagem de fundo -->
<div class="modal fade" id="AlteraImageFundo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alterar Tema de Fundo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
        	<strong><i class="fas fa-exclamation-circle"></i> Tamanho da imagem deve ser superio a 4000X6000</strong>
        </div>
        	<form method="post" action="{{ route('painel.aluno.config.trocaImageFundo')}}" enctype="multipart/form-data">
            <div class="trocaImage">
            		{!! csrf_field() !!}
            		<div class="form-group">
            			<input class="form-file" type="file" name="trocaImageFundo">
            		</div>
            </div>
            <div class="float-right mt-5">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save changes</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

<!-- 3 Modal de trocar dados pessoais -->

<div class="modal fade" id="AlteraImagePerfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alterar Imagem de Perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
        	<strong><i class="fas fa-exclamation-circle"></i> Tamanho da imagem deve ser superio a 4000X6000</strong>
        </div>
        <div class="trocaImage">
        	<form method="post" action="{{ route('painel.aluno.config.trocaImagePerfil')}}" enctype="multipart/form-data">
        		{!! csrf_field() !!}
        		<div class="form-group">
        			<input class="form-file" type="file" name="trocaImagePerfil">
        		</div>
            <div class="float-right mt-5">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
              <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- 2 Modal de trocar imagem de fundo -->
<div class="modal fade" id="AlterarDadosPessoais" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alterar Tema de Fundo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="alert alert-warning">
          	<strong><i class="fas fa-exclamation-circle"></i> Tamanho da imagem deve ser superio a 4000X6000</strong>
          </div>
          <div class="trocaImage">
            		{!! csrf_field() !!}
            		<div class="form-group">
            			<input class="form-file" type="file" name="trocaImageFundo">
            		</div>
            </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
          <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- 4 Modal de trocar endereço -->

<div class="modal fade" id="AlteraEndereco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alterar Endereço</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form class="" action="{{ route('painel.config.endereco') }}" method="post">
            <div class="row">
            <div class="col-md-9">
              <div class="form-group">
                    <label>Rua <span class="asterisco">*</span></label>
                    <input type="text" class="form-control" name="endereco" placeholder="Rua, AV, Travessa..." value="{{ auth()->user()->rua }}Rua Flamingo de Ana Gonzaga">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                  <label>Número <span class="asterisco">*</span></label>
                  <input type="text" class="form-control" name="numero" placeholder="Número" value="{{ auth()->user()->numero }}">
              </div>
            </div>
          </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label>CEP <span class="asterisco">*</span></label>
                    <input type="text" class="form-control" name="cep" placeholder="CEP" value="{{ auth()->user()->cep }}23059-492">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label>Bairro <span class="asterisco">*</span></label>
                    <input type="text" class="form-control" name="bairro" placeholder="Bairro" value="{{ auth()->user()->bairro }}Inhoaiba">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label>Cidade <span class="asterisco">*</span></label>
                    <input type="text" class="form-control" name="cidade" placeholder="Cidade" value="{{ auth()->user()->cidade }}Rio de Janeiro">
                </div>
              </div>
            </div>

          <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                    <label>Complemento</label>
                    <input type="text" class="form-control" name="complemento" placeholder="Complemento" value="{{ auth()->user()->complemento }}Antiga Rua 14 Lt 31 Qd 18">
                </div>
              </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label>Estado <span class="asterisco">*</span></label>
                  <input type="text" class="form-control" name="estado" placeholder="Estado" value="{{ auth()->user()->estado }}Rio de Janeiro">
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                <div class="float-right">
                  <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                  <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save changes</button>
                </div>
              </div>
          </div>
          </form>
      </div>
    </div>
  </div>
</div>
<!-- 5 Modal de trocar senha -->
<div class="modal fade" id="AlteraSenha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alteração de Senha</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
        	<strong><i class="fas fa-exclamation-circle"></i> A senha deve ter no máximo 6 caracteres</strong>
        </div>
        <div class="trocaImage">
        	<form method="post" action="{{ route('painel.config.senha') }}" enctype="multipart/form-data">
        		{!! csrf_field() !!}
        		<div class="form-group">
        			<!--<span><i class="fas fa-lock"></i></span>--><input class="form-control" type="password" name="antiga_senha" placeholder="Senha Atual">
        		</div>
        		<div class="form-group">
        			<!--<i class="fas fa-user-lock"></i> --><input class="form-control" type="password" name="nova_senha" placeholder="Nova senha">
        		</div>
        		<div class="form-group">
        			<!--<i class="fas fa-lock-open"></i> --><input class="form-control" type="password" name="confirma_senha" placeholder="Confirma senha">
        		</div>
        		<div class="float-right">
        			<button class="btn btn-success" type="submit"><i class="fas fa-save"></i> Salvar</button>
        		</div>
        	</form>
        </div>
      </div>
    </div>
  </div>
</div>


@stop
