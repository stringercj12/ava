@extends('layouts.app')

@section('title', 'Escola Interativa')
@section('logo', 'Escola Interativa')

@section('content')
<div class="container">
	<section id="breadcrumb">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item text-secondary"><a href="{{  route('admin.home.teste') }}"><i class="fas fa-home"></i> Dashboard</a></li>
				<li class="breadcrumb-item text-secondary"><i class="fas fa-home"></i> <a href="#">Protocolos</a></li>
				<li class="breadcrumb-item active text-secondary"><i class="fas fa-home"></i> Abertura</li>
			</ol>
		</nav>
	</section><!-- section breadcrumb -->


<div class="card">
	<form method="post" action="{{ route('painel.protocolo.abertura') }}">
		<div class="card-header">
			<a href="#" class="btn btn-outline-secondary">Voltar</a>
			<a href="#" class="btn btn-outline-info">Editar</a>
			<a href="#" class="btn btn-outline-success">Confirmar</a>
		</div>
		<div class="card-body">
			{!! csrf_field() !!}
			<div class="form-group">
				<div class="row">
					<div class="col-md-8">
		    			<select class="form-control" name="titulo">

		    				<label>Qual Serviço deseja solicitar ?</label>
		    				<option hidden="">-- Solicitar Serviço --</option>
		    				<option value="REVISÃO DE NOTA">REVISÃO DE NOTA</option>
		    				<option value="ENTREGA DE CONTRATO DE ESTÁGIO">ENTREGA DE CONTRATO DE ESTÁGIO</option>
		    				<option value="ENTREGAR DOCUMENTO">ENTREGAR DOCUMENTO</option>
		    				<option value="DECLARAÇÃO ESCOLAR(RIOCARD)">DECLARAÇÃO ESCOLAR(RIOCARD)</option>
		    			</select>
		    		</div>
		    		<div class="col-md-4">
		    			<select class="form-control" name="tipo_solicitacao">
		    				<label>Tipo de Solicitação</label>
		    				<option hidden="">-- Tipo De Solicitação --</option>
		    				<option value="Solicitação">Solicitação</option>
		    				<option value="Reclamação">Reclamação</option>
		    				<option value="Inclusão">Inclusão</option>
		    				<option value="Informação">Informação</option>
		    				<option value="Outros">Outros</option>
		    				<option value="Retirada">Retirada</option>
		    			</select>
		    		</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-8">
						<label>Solicitante</label>
						<input class="form-control disabled" disabled="" type="text" name="id_solicitante" placeholder="{{ auth()->user()->name }}" value="{{ auth()->user()->id }}">			
						</div>
		    		<div class="col-4">
		    			<label>Nível de Dificulade</label>
		    			<select class="form-control" name="nivel_de_dificuldade">
		    				<option hidden="">-- Nível de Dificuldade --</option>
		    				<option value="Alto">Alto</option>
		    				<option value="Médio">Médio</option>
		    				<option value="Baixo">Baixo</option>
		    			</select>
		    		</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4">
						<label>Data Abertura</label>
						<input class="form-control" type="text" name="data_abertura" value="{{ date('d/m/Y') }}">
					</div>
					<div class="col-4">
						<label>Previsão de Entrega</label>
						<input class="form-control" type="text" name="data_prevista" value="{{ date('d/m/Y') }}">
					</div>
					<div class="col-4">
						<label>Status</label>
						<input class="form-control disabled" disabled="" type="text" name="status" value="Pendente">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Observação</label>
				<textarea placeholder="Observação" rows="5" cols="5" class="form-control" type="text" name="descricao"></textarea>
				<span>1000 caraceteres</span>
			</div>
			<div class="form-group">
				<div class="row">
		    		<div class="col-md-6">
		    			<label>Anexo 1</label>
		    			<div class="custom-file">
						  <input type="file" class="custom-file-input" id="customFileLangHTML" name="anexo1">
						  <label class="custom-file-label" for="customFileLangHTML" data-browse="Permitido: JPG, JPEG, PDF, DOC">Permitido: JPG, JPEG, PDF, DOC</label>
						</div>
		    		</div>
		    		<div class="col-md-6">
		    			<label>Anexo 2</label>
		    			<div class="custom-file">
						  <input type="file" class="custom-file-input" id="customFileLangHTML" name="anexo2">
						  <label class="custom-file-label" for="customFileLangHTML" data-browse="Permitido: JPG, JPEG, PDF, DOC">Permitido: JPG, JPEG, PDF, DOC</label>
						</div>
		    		</div>
				</div>
			</div>
		</div>
	</form>
</div>



</div><!-- div container -->
@stop