@extends('layouts.app')

@section('title', 'Escola Interativa')
@section('logo', 'Escola Interativa')

@section('content')
<div class="container">
	<section id="breadcrumb">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item text-secondary"><a href="{{  route('painel.home.index') }}"><i class="fas fa-home"></i> Dashboard</a></li>
				<li class="breadcrumb-item active text-secondary"><i class="fas fa-file"></i> Situação Curricular</li>
			</ol>
		</nav>
	</section>

    <div class="text-center">
        <h2 class="my-3"><i class="fas fa-file"></i> Situação Curricular</h2>
    </div>


	<div class="card">
		<div class="card-header">
			<h4>Dados do aluno</h4>
		</div>
		<div class="card-body">
			<h5>Tabela com todas as materias do aluno</h5>
		</div>
	</div>




</div>

@stop
