@extends('layouts.app')

@section('title', 'Escola Interativa')
@section('logo', 'Escola Interativa')

@section('content')
<div class="container">
	<section id="breadcrumb">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item text-secondary"><a href="{{  route('painel.home.index') }}"><i class="fas fa-home"></i> Dashboard</a></li>
				<li class="breadcrumb-item active text-secondary"><i class="fas fa-home"></i> Protocolos</li>
			</ol>
		</nav>
	</section>

	@if(session('success'))
		<div class="alert alert-success">
			{{ session('success')}}
		</div>
	@endif
	@if(session('error'))
		<div class="alert alert-danger">
			{{ session('error')}}
		</div>
	@endif

	<div class="card">
		<div class="card-header">
			<span class="h5">Meus Processos / Tickets</span>
			<span class="filtro-protocolo float-right d-flex">
				<form class="form-row mr-2">
					<select class="form-control">
						<option hidden>Filtros</option>
						<option value="Aberto">Aberto</option>
						<option value="Andamento">Andamento</option>
						<option value="Fechado">Fechado</option>
					</select>
				</form>
				<button class="btn btn-outline-success btn-sm ml-2" data-toggle="modal" data-target="#NovoProtoccolo">
					<i class="fas fa-plus"></i> Novo
				</button>
			</span>
		</div>

		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<h4>Protocolos</h4>
					<div class="qtd-protocolos">
						<h6>Total de tickets: {{$total}} </h6>
					</div>
					<div class="accordion" id="accordionProtocolo">
						@if($protocolos->count() == 0)
						  	<div class="alert alert-secondary">
						  		<strong>Nenhum protocolo aberto no momento</strong>
						  	</div>
						@else
							@foreach($protocolos as $protocolo)
							  	<div class="card">
								    <div class="card-header" id="heading{{ $protocolo->id }}">
								      <h2 class="mb-0 d-flex justify-content-between align-items-center">
								        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $protocolo->id }}" aria-expanded="true" aria-controls="collapse{{ $protocolo->id }}">
								          {{ $protocolo->id }} - {{ $protocolo->titulo }}
										</button>
										<button class="btn btn-link">
											<i class="fa fa-ellipsis-h"></i>
										</button>
								      </h2>
								    </div>

								    <div id="collapse{{ $protocolo->id }}" class="collapse" aria-labelledby="heading{{ $protocolo->id }}" data-parent="#accordionProtocolo">
								      <div class="card-body">
								      	<table class="table">
											<thead>
												<tr class="text-center">
													<th>Tipo</th>
													<th>Data</th>
													<th>Situação</th>
													<th>Ações</th>
												</tr>
											</thead>
											<tbody>
												<tr class="text-center">
													<td>{{ $protocolo->tipo_solicitacao }}</td>
													<td>{{ $protocolo->data_abertura }}</td>
													{{-- <td style="word-break: break-word;">
														{{ $protocolo->descricao }}
													</td> --}}
													<td>{{ $protocolo->status }}</td>
													<td class="text-center dropdown">
														<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<i class="fas fa-clone"></i>
														</a>
														
														<div class="dropdown-menu">
															{{-- <a class="dropdown-item" href="#">
																Vizualizar
															</a> --}}
															<a href="#cancelar" modal-toggle="cancelar" data-target="#cancelar" class="dropdown-item">
																Cancelar
															</a>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
								      </div>
								    </div>
							  	</div>
						  	@endforeach
					  	@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Novo Protocolo -->

<!-- Modais do menu -->

<div class="modal fade show" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				Informe o motivo do cancelamento ?
			</div>
			<div class="modal-body">
				<form action="#" method="post">
					<div class="form-group">
						<input type="text" class="form-control" name="motivoCancelamento">
					</div>
					<button type="submit" class="btn btn-sm btn-outline-danger">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- 1 Modal de trocar imagem perfil -->
<div class="modal fade show" id="NovoProtoccolo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Abertura de Ticket</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="p-2 mb-3 alert-warning">
        	<strong><i class="fas fa-exclamation-circle"></i> Atenção: Protocolos sem informações claras não serão tratados.</strong>
        </div>
        <div>
        	<form method="post" action="{{ route('painel.protocolo.abertura') }}">
        		{!! csrf_field() !!}
        		<div class="form-group">
        			<div class="row">
        				<div class="col-md-8">
        					<label>Qual Serviço deseja solicitar ?</label>
		        			<select class="form-control" name="titulo">
		        				<option hidden="">-- Solicitar Serviço --</option>
		        				<option value="REVISÃO DE NOTA">REVISÃO DE NOTA</option>
		        				<option value="ENTREGA DE CONTRATO DE ESTÁGIO">ENTREGA DE CONTRATO DE ESTÁGIO</option>
		        				<option value="ENTREGAR DOCUMENTO">ENTREGAR DOCUMENTO</option>
		        				<option value="DECLARAÇÃO ESCOLAR(RIOCARD)">DECLARAÇÃO ESCOLAR(RIOCARD)</option>
		        			</select>
		        		</div>
		        		<div class="col-md-4">
		        			<label>Tipo de Solicitação</label>
		        			<select class="form-control" name="tipo_solicitacao">
		        				<option hidden="">-- Tipo De Solicitação --</option>
		        				<option value="S">Solicitação</option>
		        				<option value="R">Reclamação</option>
		        				<option value="IC">Inclusão</option>
		        				<option value="I">Informação</option>
		        				<option value="O">Outros</option>
		        				<option value="RE">Retirada</option>
		        			</select>
		        		</div>
        			</div>
        		</div>
        		<div class="form-group">
        			<div class="row">
	        			<div class="col-8">
	        				<label>Solicitante</label>
	        				<input class="form-control disabled" disabled="" type="text" placeholder="{{ auth()->user()->name }}">			
	      					<input class="form-control disabled" type="hidden" name="user_id" value="{{ auth()->user()->id }}">			
	      				</div>
		        		<div class="col-4">
		        			<label>Nível de Dificulade</label>
		        			<select class="form-control" name="nivel_de_dificuldade">
		        				<option hidden="">-- Nível de Dificuldade --</option>
		        				<option value="Alto">Alto</option>
		        				<option value="Médio">Médio</option>
		        				<option value="Baixo">Baixo</option>
		        			</select>
		        		</div>
        			</div>
        		</div>
        		
        		<div class="form-group">
        			<div class="row">
        				<div class="col-md-4">
	        				<label>Data Abertura</label>
	        				<input class="form-control" type="text" name="data_abertura" value="{{ date('Y-m-d') }}">
	        			</div>
	        			<div class="col-4">
	        				<label>Previsão de Entrega</label>
	        				<input class="form-control" type="text" name="data_prevista" value="{{ date('Y-m-d', strtotime("+7 days")) }}">
	        			</div>
	        			<div class="col-4">
	        				<label>Status</label>
        					<input class="form-control disabled" disabled="" type="text" value="Pendente">
        					<input class="form-control disabled" type="hidden" name="status" value="Pendente">
	        			</div>
        			</div>
        		</div>
        		<div class="form-group">
        			<label>Observação</label>
        			<textarea placeholder="Observação" rows="5" cols="5" class="form-control" type="text" min="0" name="descricao" v-on:keyup="calc" v-model="message"></textarea>
				<span v-class="{'text-danger': hasError}">@{{ valorRestante }} caraceteres</span>
        		</div>
        		<div class="form-group">
        			<div class="row">
		        		<div class="col-md-6">
		        			<label>Anexo 1</label>
		        			<div class="custom-file">
							  <input type="file" class="custom-file-input" id="customFileLangHTML" name="anexo1">
							  <label class="custom-file-label" for="customFileLangHTML" data-browse="Permitido: JPG, JPEG, PDF, DOC">Permitido: JPG, JPEG, PDF, DOC</label>
							</div>
		        		</div>
		        		<div class="col-md-6">
		        			<label>Anexo 2</label>
		        			<div class="custom-file">
							  <input type="file" class="custom-file-input" id="customFileLangHTML2" name="anexo2">
							  <label class="custom-file-label" for="customFileLangHTML2" data-browse="Permitido: JPG, JPEG, PDF, DOC">Permitido: JPG, JPEG, PDF, DOC</label>
							</div>
		        		</div>
        			</div>
        			
        		</div>
		        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
		        <button type="submit" id="btnEnviar" class="btn btn-success"><i class="fas fa-save"></i> Salvar</button>
        	</form>
        </div>
      </div>
      <div class="modal-footer">
      	<div class="rodape">Form</div>
      </div>
    </div>
  </div>
</div>
@stop

