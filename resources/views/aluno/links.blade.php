@extends('layouts.app')

@section('title', 'Escola Interativa')
@section('logo', 'Escola Interativa')

@section('content')
<div class="container">
	<section id="breadcrumb">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item text-secondary"><a href="{{  route('painel.home.index') }}"><i class="fas fa-home"></i> Dashboard</a></li>
				<li class="breadcrumb-item active text-secondary"><i class="fas fa-archive"></i> Links Úteis</li>
			</ol>
		</nav>
	</section>

    <div class="text-center">
        <h2 class="my-3"><i class="fas fa-archive"></i> Links Úteis</h2>
    </div>


	<div class="card">
		<div class="card-header">
			<h4>Links Para Acesos</h4>
		</div>
		<div class="card-body">
			<table class="table">
				<tr>
					<th>#</th>
					<th>Nome da página</th>
					<th>Observações</th>
					<th>Link</th>
					<th>Ação</th>
				</tr>
				<tr>
					<td>1</td>
					<td>Biblioteca</td>
					<td>Link para acesso a biblioteca geral da EI.</td>
					<td><a href="http://stringercj13.5gbfree.com/livraria" target="_blank">http://stringercj13.5gbfree.com/livraria</a></td>
					<td><a href="http://stringercj13.5gbfree.com/livraria" target="_blank"><i class="fas fa-external-link-alt"></i></a></td>
				</tr>
			</table>
		</div>
	</div>
X67JJ-NCXVR-K4T6H-R2Q8J-D9MQR
</div>

@stop
