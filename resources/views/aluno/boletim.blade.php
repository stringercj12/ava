@extends('layouts.app')

@section('title', 'Meu Boletim')
@section('logo', 'Escola Interativa')

@section('content')
    <div class="container">
        <section id="breadcrumb">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item text-secondary">
                        <a href="#"><i class="fas fa-home"></i> Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active text-secondary">
                        <i class="fas fa-file"></i> Boletim
                    </li>
                </ol>
            </nav>
        </section>

        <div class="text-center">
            <h2 class="my-3"><i class="fas fa-address-card"></i> Meu Boletim</h2>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8">
                                <span>Selecione o Curso/Período</span>
                                <form class="form-inline">
                                    <div class="form-group">
                                        {!! csrf_field() !!}
                                        <select class="form-control" name="boletim">
                                            <option hidden="">-- Selecione --</option>
                                            <option value="">SELECIONAR TODOS</option>
                                            @forelse($totalcursos as $boletim)
                                                <option value="Período">Período</option>
                                            @empty
                                                <option value="Nenhum período cadastrado">Nenhum período cadastrado</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </form>
                            </div>

                           <div class="col-md-4">
                               <div class="float-right">
                                   <button onclick="window.open()" class="btn btn-outline-dark"><i class="fas fa-print"></i> imprimir</button>
                               </div>
                           </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="info-aluno alert alert-info">
                            <table>
                                <tr>
                                    <th>Aluno:</th>
                                    <th>Matrícula:</th>
                                </tr>
                                <tr>
                                    <td>{{auth()->user()->name}}</td>
                                     <td>{{ auth()->user()->id}}</td>
                                </tr>
                                <tr>
                                    <th>Curso:</th>
                                    <th>Período:</th>
                                </tr>
                                <tr>
                                    <td>ASP NET Core 2</td>
                                    <td>4º Período</td>
                                </tr>
                            </table>
                        </div>
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th>Disciplina</th>
                                    <th>AV1</th>
                                    <th>AV2</th>
                                    <th>AV3</th>
                                    <th>APS</th>
                                    <th>Média</th>
                                    <th>Falta Geral</th>
                                    <th>Nota Final</th>
                                    <th>Resultado</th>
                                </tr>
                            </thead>

                            <tbody>
                                @for($i = 0; $i < 4; $i++)
                                    <tr>
                                        <td>Nome da Disciplina</td>
                                        <td>8.5</td>
                                        <td>7</td>
                                        <td>0</td>
                                        <td>2</td>
                                        <td>{{ (8.5 + 7)/2 }}</td>
                                        <td>10</td>
                                        <td>7,75</td>
                                        <td>Aprovado</td>
                                        <td>
                                    <a href="#" class="btn btn-sm btn-outline-dark">Teste</a></td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop
