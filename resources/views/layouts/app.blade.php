<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/meu-estilo.css') }}" rel="stylesheet">
    <link href="{{ asset('css/navbar.css') }}" rel="stylesheet">
    <!-- Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Styles do navbar -->
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <button class="btn btn-outline-primary btn-sm ml-3 d-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-th"></i></button>
            <div class="container">
                <a href="" class="text-success btn-link fa-th fas" style="font-size: 23px;text-decoration: none;" data-toggle="modal" data-target="#exampleModal"></a>
                <a class="navbar-brand mx-3" href="{{ url('/painel') }}">
                    @yield('logo')
                </a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Fim teste -->
                        <li class="nav-item dropdown ml-3">
                            <a id="IdNotificacao" href="#" class="nav-link nav-hover mt-1" data-toggle="dropdown"  style="position: relative;">
                                <i class="fas fa-bell"></i>
                                <i id="alertas">5</i>
                            </a>
                                @if(auth()->user()->remember_token)
                                    <ul class="dropdown-menu dropdown-menu-right btn-block bg-white" aria-labelledby="IdNotificacao">
                                        <li class="nav-item ml-2">
                                            <i class="fas fa-bell"></i> Sem Notificação
                                        </li>
                                    </ul>
                                @else
                                    <div class="dropdown-menu dropdown-menu-right" style="width: 300px">
                                        <div class="notifica">
                                            <div class="header">
                                                <h5 class="text-secondary" style="font-weight: bold;">Você tem 5 mensagens</h5> <!-- Info qtd de msg nova -->
                                            </div>
                                            <div class="msg-item">
                                                @for($i = 0; $i <= 2; $i++)
                                                    <a href="#" class="notifica-item align-items-center d-flex justify-content-center">
                                                        <div class="mr-1">
                                                            <img src="{{ auth()->user()->image }}" alt="User Image" class="circle notificacao-img"> <!-- img do user que mandou a mensagem-->
                                                        </div>
                                                        <div class="texto">
                                                            <h6 style="font-weight: bold;">Equipe de suporte <small class="icon-minutes"> <i class="far fa-clock"></i> {{$i}} mins</small></h6> <!-- titulo da msg -->
                                                            <small>Por que não comprar um novo tema incrível?</small> <!-- mini corpo da msg -->
                                                        </div>
                                                    </a>
                                                <div class="text-secondary" style="border-bottom: 0.05rem solid #ccc"></div>
                                                @endfor
                                            </div>

                                            <div class="rodape">
                                                <a href="#">Ver todas as notificações</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                        </li>
                        <li class="nav-item dropdownMsg ml-3">
                            <a href="#" class="nav-link mt-1 nav-hover" data-toggle="dropdownMsg"  style="position: relative;">
                                <i class="fas fa-envelope"></i>
                                <span id="menssage">2</span>
                            </a>

                            @if(auth()->user()->remember_token)
                                    <ul class="dropdown-menu dropdown-menu-right btn-block bg-white" aria-labelledby="IdNotificacao">
                                        <li class="nav-item">
                                            <i class="fas fa-bell"></i> Sem Notificação
                                        </li>
                                    </ul>
                                @else
                                    <div class="dropdown-menu dropdown-menu-right" style="width: 300px">
                                        <div class="notifica">
                                            <div class="header">
                                                <h5 class="text-secondary" style="font-weight: bold;">Você tem 5 mensagens</h5> <!-- Info qtd de msg nova -->
                                            </div>
                                            <div class="msg-item">
                                                @for($i = 0; $i <= 2; $i++)
                                                    <a href="#" class="notifica-item align-items-center d-flex justify-content-center">
                                                        <div class="mr-1">
                                                            <img src="{{ auth()->user()->image }}" alt="User Image" class="circle notificacao-img"> <!-- img do user que mandou a mensagem-->
                                                        </div>
                                                        <div class="texto">
                                                            <h6 style="font-weight: bold;">Equipe de suporte <small class="icon-minutes"> <i class="far fa-clock"></i> {{$i}} mins</small></h6> <!-- titulo da msg -->
                                                            <small>Por que não comprar um novo tema incrível?</small> <!-- mini corpo da msg -->
                                                        </div>
                                                    </a>
                                                <div class="text-secondary" style="border-bottom: 0.05rem solid #ccc"></div>
                                                @endfor
                                            </div>

                                            <div class="rodape">
                                                <a href="#">Ver todas as mensagens</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                        </li>
                        <li class="nav-item dropdown mr-2">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle nav-hover" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span class="m-auto">
                                    <img class="img-fluid" id="imgUser" src="{{ auth()->user()->image }}" alt="{{ auth()->user()->name }}" style="border-radius: 50%; width: 35px; height: 35px; border: 1px solid #212529;">
                                    <span id="nomeUser">{{ auth()->user()->name }}</span>
                                    <span class="caret"></span>
                                </span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right btn-block" aria-labelledby="navbarDropdown">
                                <a href="{{ route('painel.aluno.perfil') }}" class="dropdown-item">
                                    <i class="fas fa-address-card"></i> Meu Perfil
                                </a>

                                <a href="{{ route('config')}}" class="dropdown-item">
                                    <i class="fas fa-cog"></i> Configurções
                                </a>

                                <a class="dropdown-item" href="{{ route('logout') }}">
                                   <i class="fas fa-sign-out-alt"></i> Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>

                <!-- Modal Menu -->
                <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="bg-white" role="document" style="max-width: 300px; height: 100%;">
                        <div class="modal-content" style="border:none;">
                            <div style="position: absolute;right: 7px;top: 12px;">
                                <button type="button" class="close cor-padrao" data-dismiss="modal" aria-label="Close">
                                  <i class="far fa-times-circle"></i>
                                </button>
                            </div>
                            <div class="menu-modal">
                                <div class="img-fundo">
                                    <ul class="menu-modal-perfil p-0 text-center" style="list-style: none;">
                                        <li class="menu-img nav-item">
                                            <a href="{{ route('painel.aluno.perfil') }}">
                                                <img src="{{ auth()->user()->image }}" alt="img Perfil" style="width: 64px;height: 64px;border-radius: 50%;">
                                            </a>
                                        </li>
                                        <li class="menu-text-name nav-item">
                                            <a href="#" class="menu-modal-link-btn menu-modal-link-perfil"><i class="fas fa-user"></i> {{ auth()->user()->name }}</a>
                                        </li>
                                        <li class="menu-text-email nav-item">
                                            <a href="#" class="menu-modal-link-btn menu-modal-link-perfil"><i class="fas fa-envelope"></i> {{ auth()->user()->email }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!-- <div style="border: 0.05rem solid #ccc;margin: 10px 0px;"></div> -->

                            <div class="btn-block menu-modal-list" style="display: block;width: 100%;">
                                @if(auth()->user()->tipoUsuario === "Administrador")
                                <!-- ADmin -->Admin
                                    <ul class="menu-modal-lista">
                                        <li class="lista-item">
                                            <a href="{{ route('painel.home.index')}}" class="lista-link">
                                                <i class="fas fa-home"></i> Início
                                            </a>
                                        </li>
                                         <li class="lista-item">
                                            <a href="#" class="lista-link">
                                                <i class="fas fa-user-graduate"></i> Alunos
                                            </a>
                                        </li>
                                        <li class="lista-item">
                                            <a href="#" class="lista-link">
                                                <i class="fas fa-cash-register"></i> Faturamento
                                            </a>
                                        </li>
                                         <li class="lista-item">
                                            <a href="#" class="lista-link">
                                                <i class="fas fa-address-card"></i> Nova Mátricula
                                            </a>
                                        </li>
                                    </ul>
                                @else
                                <!-- Aluno -->
                                    <form class="form-control-sm mt-3">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <input type="text" name="busca" placeholder="Buscar" class="form-control form-busca-menu">
                                        </div>
                                    </form>
                                    <ul class="menu-modal-lista mt-4">
                                        <li class="lista-item">
                                            <a href="{{ route('painel.home.index') }}" class="lista-link">
                                                <i class="fas fa-home"></i> Início
                                            </a>
                                        </li>
                                        <li class="lista-item d-md-none d-block">
                                            <a href="{{ route('painel.aluno.perfil') }}" class="lista-link">
                                                <i class="fas fa-address-card"></i> Meu Perfil
                                            </a>
                                        </li>
                                         <li class="lista-item">
                                            <a href="{{ route('painel.aluno.protocolos') }}" class="lista-link">
                                                <i class="fas fa-user-graduate"></i> Protocolos
                                            </a>
                                        </li>
                                        <li class="lista-item">
                                            <a href="{{ route('painel.aluno.boletim') }}" class="lista-link">
                                                <i class="fas fa-cash-register"></i> Boletim
                                            </a>
                                        </li>
                                         <li class="lista-item">
                                            <a href="#" class="lista-link">
                                                <i class="fas fa-address-card"></i> Consulta Frequência
                                            </a>
                                        </li>
                                         <li class="lista-item">
                                            <a href="{{ route('painel.aluno.situacao') }}" class="lista-link">
                                                <i class="fas fa-address-card"></i> Situação Curricular
                                            </a>
                                        </li>
                                         <li class="lista-item">
                                            <a href="{{ route('painel.aluno.atividades') }}" class="lista-link">
                                                <i class="fas fa-address-card"></i> Ativid. Complementares
                                            </a>
                                        </li>
                                         <li class="lista-item">
                                            <a href="#" class="lista-link">
                                                <i class="fas fa-address-card"></i> Rematrícula
                                            </a>
                                        </li>

                                         <li class="lista-item">
                                            <a href="#" class="lista-link">
                                                <i class="fas fa-address-card"></i> Turma
                                            </a>
                                        </li>

                                         <li class="lista-item">
                                            <a href="#" class="lista-link">
                                                <i class="fas fa-address-card"></i> Central de Downloads
                                            </a>
                                        </li>

                                        <li class="lista-item">
                                            <a href="{{ route('painel.aluno.links') }}" class="lista-link">
                                                <i class="fas fa-address-card"></i> Links
                                            </a>
                                        </li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>

        <footer>
            @yield('footer')
        </footer>
    </div>

    <script type="text/javascript">
        $('#exampleModal').modal({
          keyboard: true,
          focus:true,
        });
    </script>
</body>
</html>
