@extends('layouts.app')

@section('title', 'Escola Interativa')
@section('logo', 'Escola Interativa')

@section('content')
      <style type="text/css">
            /*.bg-primary{
                  background-color: green;
            }
            .IdNotificacao{    position: absolute;
                right: 5%;
                left: auto;
                border: 1px solid #ddd;
                background: #fff;
            }
            .IdNotificacao .header{
                  border-top-left-radius: 4px;
                      border-top-right-radius: 4px;
                      border-bottom-right-radius: 0;
                      border-bottom-left-radius: 0;
                      background-color: #ffffff;
                      padding: 7px 10px;
                      border-bottom: 1px solid #f4f4f4;
                      color: #444444;
                      font-size: 14px;
            }*/
            .IdNotificacao{
                  list-style: none;
            }

            .IdNotificacao .header{
                  color: #404b83 !important;
            }
            .circle{
                  width: 34px !important;
                  height: 34px !important;
                  border-radius: 50% !important;
            }
            .footer{
                  text-align: center;
            }
      </style>

      <div class="container bg-primary">
                 @foreach ($alertas as $alerta )
            <ul class="list-group">
                      <div class="dropdown-menu dropdown-menu-right" style="width: 300px">
                          <ul class="list-group notificacao-area">
                              <li class="header text-secondary list-group-item"><h5>Você tem {{ $alerta->titulo }} mensagens</h5></li> <!-- Info qtd de msg nova -->
                              <li class="list-group-item">
                                  <div class="align-items-center d-flex justify-content-center">
                                      <div class="col-md-4">
                                          <img src="{{ auth()->user()->image }}" alt="User Image" class="circle notificacao-img"> <!-- img do user que mandou a mensagem-->
                                      </div> 
                                      <div class="col-md-10">
                                          <h6>Equipe de suporte <small class="icon-minutes"> <i class="far fa-clock"></i> {{ $alerta->texto }} mins</small></h6> <!-- titulo da msg -->
                                          <small>Por que não comprar um novo tema incrível?</small> <!-- mini corpo da msg -->
                                      </div>
                                  </div>
                              </li> 
                              <li class="footer list-group-item text-center">
                                      <a href="{{ route('admin.chat') }}">Ver todas as mensagens</a>
                              </li>
                          </ul>
                      </div>
                      <ul class="dropdown-menu dropdown-menu-right btn-block bg-white" aria-labelledby="IdNotificacao">
                          <li class="nav-item">
                              <i class="fas fa-bell"></i> Sem Notificação
                          </li>
                      </ul>
            </ul>
                  @endforeach
                  
      </div> 
@stop