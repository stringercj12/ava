<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProtocolosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('protocolos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->text('descricao');
            $table->string('anexo1')->nullable();
            $table->string('anexo2')->nullable();
            $table->string('tipo_solicitacao');
            $table->unsignedBigInteger('user_id');
            $table->string('status');
            $table->string('nivel_de_dificuldade');
            $table->date('data_abertura');
            $table->date('data_prevista');
            $table->date('data_fechamento')->nullable();
            $table->text('resolucao')->nullable();
            $table->string('anexo_resolucao')->nullable();
            $table->timestamps();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropForeign('protocolos_user_id_foreign');
        Schema::dropIfExists('protocolos');
    }
}
