<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        //Apaga todos os dados da tabela
        //DB::table('users')->truncate();

        // $faker = Faker::create();
        //
        // foreach(range(1,100) as $i)
        // {
        //     User::create([
        //         'name' => $faker->name(),
        //         'email' => $faker->email(),
        //         'image' => $faker->image(),
        //         'tipoUsuario' => 'Aluno',
        //         'rua' => '',
        //         'numero' => '',
        //         'cep' => '',
        //         'bairro' => '',
        //         'cidade' => '',
        //         'estado' => '',
        //         'password' => bcrypt($faker->word()),
        //     ]);
        // }

        User::create([
            'id' => 1,
            'name' => 'Jefferson Ferreira',
            'email' => 'jefferson14489@gmail.com',
            'image' => 'https://images.unsplash.com/photo-1553830591-d8632a99e6ff?ixlib=rb-1.2.1&auto=format&fit=crop&w=911&q=80',
            'tipoUsuario' => 'Administrador',
            'password' => bcrypt('123'),
        ]);

        User::create([
            'id' => 2,
            'name' => 'Paula Ferreira',
            'email' => 'paula.reis@live.com',
            'image' => 'https://images.unsplash.com/photo-1521310192545-4ac7951413f0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
            'tipoUsuario' => 'Aluno',
            'password' => bcrypt('123'),
        ]);

        \App\Eventos::create([
            'name' => 'Etapa 2: Entrega da apresentação (PPT) do TCC',
            'detalhes' => 'Entrega de documentos para apresentação',
            'start' => '2019-06-22',
            'end' => '2019-12-12',
            'color' => '#f38f39',
            'background' => '#f93e02',
            'status' => 'Confirmado',
            'user_id' => '1',
        ], [
            'name' => 'Início das Aulas dos Cursos de Extensão em Letras Números, Física e Bites & Bytes',
            'detalhes' => 'Inicio das aulas do curso',
            'start' => '2019-06-22',
            'end' => '2019-12-12',
            'color' => '#f38f39',
            'background' => '#f93e02',
            'status' => 'Cancelado',
            'user_id' => '1',
        ], [
            'name' => 'Avaliação Institucional (Turmas Presenciais)',
            'detalhes' => 'Avaliação da universidade e do semestre',
            'start' => '2019-10-12',
            'end' => '2019-12-20',
            'color' => '#f38f39',
            'background' => '#f93e02',
            'status' => 'Pendente',
            'user_id' => '1',
        ]);

    }
}
