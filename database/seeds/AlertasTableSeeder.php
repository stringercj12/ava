<?php

use Illuminate\Database\Seeder;
use App\Alertas;

class AlertasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Apaga todos os dados da tabela
        // DB::table('users')->truncate();

       Alertas::create([
	        'titulo' => 'Titulo do Alerta',
	        'texto' => 'Texto da notificacao ou alerta',
	        'status' => 'N',
	   ]);
       Alertas::create([
            'titulo' => 'Jefferson Ferreira',
            'texto' => 'jefferson14489@gmail.com',
            'status' => 'N',
        ]);
    }
}
