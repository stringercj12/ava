<?php

use Illuminate\Database\Seeder;
use App\Cursos;

class CursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//Apaga todos os dados da tabela
        // DB::table('cursos')->truncate();

        Cursos::create([
        	'id' => '456',
        	'name' => 'SISTEMAS OPERACIONAIS',
    			'categoria' => 'Informatica',
    			'duracao' => '1.280',
    			'descricao' => 'Nesta disciplina serão apresentados o funcionamento, a estrutura, a organização e o desempenho dos computadores, enfatizando os aspectos técnicos-operacionais do ambiente de software básico, conceitos básicos sobre sistemas operacionais multiprogramáveis, de modo a permitir a base de conhecimentos requeridos para o aluno compreender as disciplinas práticas sobre sistemas operacionais do mercado.'
    		]);

    		Cursos::create([
            'id' => '356',
            'name' => 'ALGORITMOS II',
      			'categoria' => 'Informatica',
      			'duracao' => '3.339',
      			'descricao' => 'Neste curso veremos uma introdução a algoritmos, utilizando como linguagem de apoio o Portugol. Para isso, abordaremos assuntos como descrição narrativa, fluxogramas e pseudocódigos, fundamentais para quem está iniciando na programação. O curso aborda tópicos iniciais no processo de elaboração de algoritmos a partir da descrição narrativa, fluxogramas e pseudocódigos. Como linguagem para o desenvolvimen Iniciar agora Carga horária deste curso 16h',
      		]);

      		Cursos::create([
              	'id' => '789',
              	'name' => 'RACIOCÍNIO LÓGICO',
      			'categoria' => 'Informatica',
      			'duracao' => '2.983',
      			'descricao' => 'Nesse sentido, as propriedades operacionais são a grande contribuição do logaritmo para o nosso cotidiano, visto que são bastante utilizadas ainda hoje. Além disso, o estudo da função logarítmica é capaz de simular, por exemplo, algumas situações da Física, como a medição do decibel e a Lei de Benford.',
      		]);

      		Cursos::create([
              	'id' => '852',
              	'name' => 'PROGRAMAÇÃO ORIENTADA A OBJETO',
      			'duracao' => '4.440',
      			'categoria' => 'Informatica',
      			'descricao' => 'Neste curso são apresentados os fundamentos deste importante e amplamente utilizado paradigma de programação, por meio de explicações introdutórias e didáticas, exemplos práticos e situações de vida real.',
      		]);

      		Cursos::create([
              	'id' => '234',
              	'name' => 'TEORIA DE ENGENHARIA DE SOFTWARE',
      			'duracao' => '3.890',
      			'categoria' => 'Informatica',
      			'descricao' => 'Forma os profissionais mais valorizados do setor de software, um segmento de salários altos e vagas sobrando. O salário médio de um gerente de sistemas é de R$10 a 20 mil. Mesmo estagiários ganham bolsas significativas: a bolsa média de estágio em engenharia no Brasil é de R$2.008,00 para alunos do último ano (fonte: Exame Robert Half).',
      		]);
    }
}
